
import React, { useState, useEffect } from 'react';

import { Platform, Image, Text, View, StyleSheet, ActivityIndicator, FlatList, TouchableOpacity } from 'react-native';



export default function App() {

  const [quote, setQuote] = useState(null);

  const [quotes, setQuotes] = useState(null);

  const getQuote = () => {
    return fetch('https://thesimpsonsquoteapi.glitch.me/quotes')
      .then((response) => response.json())
      .then((data) => {
        setQuote(data[0])
      })
      .catch((error) => {
        console.error(error);
      });
  };

  const update = () => {
    getQuote();
  }

  useEffect(() => {
    (async () => {
      getQuote();
    })();
  }, []);


  let text = <ActivityIndicator />;

  if (quote) {

    text = null;

  }


  const Item = ({ title }) => (
    <View style={styles.item}>
      {/*<Image style={styles.tinyLogo} source={{uri: quote[0].image}}/>*/}
      {/*<Text style={styles.title}>Nom: {quote[0].character}</Text>*/}
      {/*<Text style={styles.title}>Citation: {quote[0].quote}</Text>*/}
    </View>
  );

  const renderItem = ({ item }) => (
    <Item title={item} />
  );

  return (

    <View style={styles.container}>

      <View style={styles.quoteCard}>
        <Text style={styles.paragraph}>{text}</Text>
        {quote &&
          <View>
            <Image style={styles.tinyLogo} source={{uri: quote.image}}/>
            <Text style={styles.title}>Nom: {quote.character}</Text>
            <Text style={styles.title}>Citation: {quote.quote}</Text>
          </View>
        }
      </View>
      <TouchableOpacity style={styles.update} onPress={() => update()}>
        <Text style={styles.btnText}>Actualiser</Text>
      </TouchableOpacity>
      {/*<FlatList data={quotes} renderItem={renderItem}/>*/}
    </View>

  );

}
const styles = StyleSheet.create({
  container: {
    flex: 1,
    alignItems: 'center',
    justifyContent: 'center',
    paddingVertical: 40,
    backgroundColor: "gray"
  },
  update:{
    padding: 10,
    backgroundColor: 'skyblue',
    marginVertical:10,
    borderRadius: 10
  },
  btnText:{
    fontSize:18
  },
  titre:{
    fontSize:26,
    fontWeight:"bold",
    marginBottom:10
  },
  paragraph: {
    fontSize: 18,
    textAlign: 'center',
  },
  tinyLogo: {
    width: 100,
    height: 175,
  },
  item: {
    backgroundColor: '#f9c2ff',
    padding: 20,
    marginVertical: 8,
    marginHorizontal: 16,
    borderRadius: 10
  },
  title: {
    fontSize: 18,
  },
  quoteCard: {
    backgroundColor: "orange",
    padding: 20,
    alignItems: 'center',
    justifyContent: 'center',
    borderRadius: 10
  }
});